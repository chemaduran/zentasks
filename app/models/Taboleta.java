package models;

/**
 * Created with IntelliJ IDEA.
 * User: jmgarcia
 * Date: 17/06/13
 * Time: 12:13
 * To change this template use File | Settings | File Templates.
 */

import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;

@Entity
public class Taboleta extends Model {


    public String Bonumero;
    public String Bofechaoperacion;
    public String Botipooperacion;
    public String Bonumerocontratos;
    public String Boprecio;

    public static Model.Finder<Long, Taboleta> find = new Model.Finder(Long.class, Taboleta.class);

    public static List<Taboleta> findInvolving() {
        return find.findList();
    }

}
