// @SOURCE:C:/chema/proyectos/WEB/play-2.1.1/samples/java/zentasks/conf/routes
// @HASH:b3a37114a2e8346f68d474df619a55e74a3aead1
// @DATE:Tue Jun 18 13:28:35 CEST 2013

package controllers;

public class routes {
public static final controllers.ReverseProjects Projects = new controllers.ReverseProjects();
public static final controllers.ReverseTasks Tasks = new controllers.ReverseTasks();
public static final controllers.ReverseApplication Application = new controllers.ReverseApplication();
public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets();
public static class javascript {
public static final controllers.javascript.ReverseProjects Projects = new controllers.javascript.ReverseProjects();
public static final controllers.javascript.ReverseTasks Tasks = new controllers.javascript.ReverseTasks();
public static final controllers.javascript.ReverseApplication Application = new controllers.javascript.ReverseApplication();
public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets();    
}   
public static class ref {
public static final controllers.ref.ReverseProjects Projects = new controllers.ref.ReverseProjects();
public static final controllers.ref.ReverseTasks Tasks = new controllers.ref.ReverseTasks();
public static final controllers.ref.ReverseApplication Application = new controllers.ref.ReverseApplication();
public static final controllers.ref.ReverseAssets Assets = new controllers.ref.ReverseAssets();    
} 
}
              