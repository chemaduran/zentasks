
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object twitter extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template1[List[Taboleta],play.api.templates.Html] {

    /**/
    def apply/*1.2*/(boletas: List[Taboleta]):play.api.templates.Html = {
        _display_ {

Seq[Any](format.raw/*1.27*/("""
<html>
    <head>
        <title>Twitter Bootstrap</title>
        <link rel="shortcut icon" type="image/png" href=""""),_display_(Seq[Any](/*5.59*/routes/*5.65*/.Assets.at("images/favicon.png"))),format.raw/*5.97*/("""">
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(Seq[Any](/*6.70*/routes/*6.76*/.Assets.at("stylesheets/login.css"))),format.raw/*6.111*/("""">
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(Seq[Any](/*7.70*/routes/*7.76*/.Assets.at("stylesheets/initializr/bootstrap.min.css"))),format.raw/*7.130*/("""">
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(Seq[Any](/*8.70*/routes/*8.76*/.Assets.at("stylesheets/initializr/bootstrap-responsive.min.css"))),format.raw/*8.141*/("""">
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(Seq[Any](/*9.70*/routes/*9.76*/.Assets.at("stylesheets/initializr/main.css"))),format.raw/*9.121*/("""">
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(Seq[Any](/*10.70*/routes/*10.76*/.Assets.at("stylesheets/jquery.dataTables.css"))),format.raw/*10.123*/("""">


        <script type="text/javascript" src=""""),_display_(Seq[Any](/*13.46*/routes/*13.52*/.Assets.at("javascripts/jquery-1.7.1.js"))),format.raw/*13.93*/(""""></script>
        <script type="text/javascript" src=""""),_display_(Seq[Any](/*14.46*/routes/*14.52*/.Assets.at("javascripts/jquery-play-1.7.1.js"))),format.raw/*14.98*/(""""></script>
        <script type="text/javascript" src=""""),_display_(Seq[Any](/*15.46*/routes/*15.52*/.Assets.at("javascripts/underscore-min.js"))),format.raw/*15.95*/(""""></script>
        <script type="text/javascript" src=""""),_display_(Seq[Any](/*16.46*/routes/*16.52*/.Assets.at("javascripts/backbone-min.js"))),format.raw/*16.93*/(""""></script>
        <script type="text/javascript" src=""""),_display_(Seq[Any](/*17.46*/routes/*17.52*/.Assets.at("javascripts/main.js"))),format.raw/*17.85*/(""""></script>

        <script type="text/javascript" src=""""),_display_(Seq[Any](/*19.46*/routes/*19.52*/.Assets.at("javascripts/initializr/vendor/bootstrap.min.js"))),format.raw/*19.112*/(""""></script>
        <script type="text/javascript" src=""""),_display_(Seq[Any](/*20.46*/routes/*20.52*/.Assets.at("javascripts/initializr/vendor/modernizr-2.6.2-respond-1.1.0.min.js"))),format.raw/*20.132*/(""""></script>
        <script type="text/javascript" src=""""),_display_(Seq[Any](/*21.46*/routes/*21.52*/.Assets.at("javascripts/initializr/plugins.js"))),format.raw/*21.99*/(""""></script>
        <script type="text/javascript" src=""""),_display_(Seq[Any](/*22.46*/routes/*22.52*/.Assets.at("javascripts/jquery.dataTables.js"))),format.raw/*22.98*/(""""></script>

        <script type="text/javascript">
        $(document).ready(function () """),format.raw/*25.39*/("""{"""),format.raw/*25.40*/("""
            $('.dropdown-toggle').dropdown();
            """),format.raw/*27.13*/("""}"""),format.raw/*27.14*/(""");


            $('#navbar').affix()

            $(document).ready(function() """),format.raw/*32.42*/("""{"""),format.raw/*32.43*/("""
            $('#example').dataTable();
            """),format.raw/*34.13*/("""}"""),format.raw/*34.14*/(""" );

        </script>

    </head>
    <body>

        <header>
            <a href=""""),_display_(Seq[Any](/*42.23*/routes/*42.29*/.Projects.index)),format.raw/*42.44*/("""" id="logo"><span>Data</span>tables</a>
        </header>


        <div id="container">
            <h1>Test</h1>
            <div id="demo">
                <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                    <thead>
                        <tr>
                            <th>Número Boleta</th>
                            <th>Fecha de Operación</th>
                            <th>Tipo de Operación</th>
                            <th>Número de contratos</th>
                            <th>Precio</th>
                        </tr>
                    </thead>
                    <tbody>

                        """),_display_(Seq[Any](/*61.26*/for(b <- boletas) yield /*61.43*/ {_display_(Seq[Any](format.raw/*61.45*/("""
                            <tr class="gradeU">
                                <td>"""),_display_(Seq[Any](/*63.38*/b/*63.39*/.Bonumero)),format.raw/*63.48*/("""</td>
                                <td>"""),_display_(Seq[Any](/*64.38*/b/*64.39*/.Bofechaoperacion)),format.raw/*64.56*/("""</td>
                                <td>"""),_display_(Seq[Any](/*65.38*/b/*65.39*/.Botipooperacion)),format.raw/*65.55*/("""</td>
                                <td class="center">"""),_display_(Seq[Any](/*66.53*/b/*66.54*/.Bonumerocontratos)),format.raw/*66.72*/("""</td>
                                <td class="center">"""),_display_(Seq[Any](/*67.53*/b/*67.54*/.Boprecio)),format.raw/*67.63*/("""</td>

                            </tr>
                        """)))})),format.raw/*70.26*/("""

                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Número Boleta</th>
                            <th>Fecha de Operación</th>
                            <th>Tipo de Operación</th>
                            <th>Número de contratos</th>
                            <th>Precio</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="spacer"></div>



        </div>

    </body>
</html>"""))}
    }
    
    def render(boletas:List[Taboleta]): play.api.templates.Html = apply(boletas)
    
    def f:((List[Taboleta]) => play.api.templates.Html) = (boletas) => apply(boletas)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Mon Jun 17 16:10:39 CEST 2013
                    SOURCE: C:/chema/proyectos/WEB/play-2.1.1/samples/java/zentasks/app/views/twitter.scala.html
                    HASH: 6e6bf68fbbe5d9944216450107462c580a2ab044
                    MATRIX: 733->1|835->26|992->148|1006->154|1059->186|1167->259|1181->265|1238->300|1346->373|1360->379|1436->433|1544->506|1558->512|1645->577|1753->650|1767->656|1834->701|1943->774|1958->780|2028->827|2117->880|2132->886|2195->927|2289->985|2304->991|2372->1037|2466->1095|2481->1101|2546->1144|2640->1202|2655->1208|2718->1249|2812->1307|2827->1313|2882->1346|2978->1406|2993->1412|3076->1472|3170->1530|3185->1536|3288->1616|3382->1674|3397->1680|3466->1727|3560->1785|3575->1791|3643->1837|3765->1931|3794->1932|3883->1993|3912->1994|4025->2079|4054->2080|4136->2134|4165->2135|4296->2230|4311->2236|4348->2251|5075->2942|5108->2959|5148->2961|5272->3049|5282->3050|5313->3059|5393->3103|5403->3104|5442->3121|5522->3165|5532->3166|5570->3182|5665->3241|5675->3242|5715->3260|5810->3319|5820->3320|5851->3329|5952->3398
                    LINES: 26->1|29->1|33->5|33->5|33->5|34->6|34->6|34->6|35->7|35->7|35->7|36->8|36->8|36->8|37->9|37->9|37->9|38->10|38->10|38->10|41->13|41->13|41->13|42->14|42->14|42->14|43->15|43->15|43->15|44->16|44->16|44->16|45->17|45->17|45->17|47->19|47->19|47->19|48->20|48->20|48->20|49->21|49->21|49->21|50->22|50->22|50->22|53->25|53->25|55->27|55->27|60->32|60->32|62->34|62->34|70->42|70->42|70->42|89->61|89->61|89->61|91->63|91->63|91->63|92->64|92->64|92->64|93->65|93->65|93->65|94->66|94->66|94->66|95->67|95->67|95->67|98->70
                    -- GENERATED --
                */
            