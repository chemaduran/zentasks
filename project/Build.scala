import sbt._
import Keys._

import play.Project._

object ApplicationBuild extends Build {

    val appName         = "zentask"
    val appVersion      = "1.0"

    val appDependencies = Seq(
      javaCore,
      javaJdbc,
      javaEbean,
      "org.firebirdsql.jdbc" % "jaybird-jdk17" % "2.2.3",
      "mysql" % "mysql-connector-java" % "5.1.18",
      "net.sourceforge.jtds" % "jtds" % "1.2"
    )

    val main = play.Project(appName, appVersion, appDependencies).settings(
      // Add your own project settings here      
    )    

}
            
